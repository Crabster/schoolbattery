package crabster.rudakov.schoolbattery;

public interface IBatteryListener {

    void onBatteryLevelChanged(BatteryInfo batteryInfo);

}
